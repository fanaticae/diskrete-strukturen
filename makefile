MK = pdflatex --output-directory=build/

script:
	mkdir build
	$(MK) Diskrete_Strukturen.tex
	$(MK) Diskrete_Strukturen.tex

.PHONY: clean
clean:
	rm -r build

uebungsblaetter.pdf: blatt_00.pdf Loesung_00.pdf blatt_01.pdf Loesung_01.pdf blatt_02.pdf Loesung_02.pdf blatt_03.pdf Loesung_03.pdf blatt_04.pdf Loesung_04.pdf blatt_05.pdf Loesung_05.pdf blatt_06.pdf Loesung_06.pdf blatt_07.pdf Loesung_07.pdf blatt_08.pdf Loesung_08.pdf blatt_09.pdf Loesung_09.pdf blatt_10.pdf Loesung_10.pdf blatt_11.pdf Loesung_11.pdf blatt_12.pdf Loesung_12.pdf
	pdfunite blatt_00.pdf Loesung_00.pdf blatt_01.pdf Loesung_01.pdf blatt_02.pdf Loesung_02.pdf blatt_03.pdf Loesung_03.pdf blatt_04.pdf Loesung_04.pdf blatt_05.pdf Loesung_05.pdf blatt_06.pdf Loesung_06.pdf blatt_07.pdf Loesung_07.pdf blatt_08.pdf Loesung_08.pdf blatt_09.pdf Loesung_09.pdf blatt_10.pdf Loesung_10.pdf blatt_11.pdf Loesung_11.pdf blatt_12.pdf Loesung_12.pdf uebungsblaetter.pdf
