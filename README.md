# README #

Diese Zusammenfassung für das Fach Diskrete Strukturen wurde im Wintersemester 2015/16 auf Basis des Skripts und der Übungsblätter der Vorlesung von Prof. Dr. Bernhard Möller erstellt.

Auf insgesamt 11 Seiten sind alle für die Klausur notwendigen Sätze, Definitionen und Aufgabenlösungen des Semesters zusammengefasst, die in unseren Augen für die OpenBook-Klausur relevant sein könnten. Da alles nicht-lebendige und nicht-elektronische mitgenommen werden darf, ist diese Zusammenfassung ein perfekter Begleiter für deinen Klausurversuch! ;)

Für Fehler übernehmen wir keine Haftung, über eine Verbesserung dieser würden sie spätere Nutzer jedoch freuen.

Unter Downloads findet ihr eine kompilierte Version der Zusammenfassung.